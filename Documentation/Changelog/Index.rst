.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _changelog:

ChangeLog
=========

All changes are documented on `https://bitbucket.org/franssaris/fs_media_gallery <https://bitbucket.org/franssaris/fs_media_gallery>`_.
Please follow the link to know which bugs have been fixed in which version.


.. _list_of_versions:

List of versions
----------------

.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   1-0-0
